/**
 * STC.Tickets
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */import { TSResellerVolumeDiscountInfo } from './tSResellerVolumeDiscountInfo';


/**
 * This type has all the information about a specific capacity slot.
 */
export interface TSResellerProductInfo { 
    resellerProductKey?: string;
    priceTypeName?: string;
    buyingPrice?: number;
    sellingPrice?: number;
    eventName?: string;
    performanceName?: string;
    performanceSectionKey?: string;
    performanceSectionName?: string;
    resellerVolumeDiscountInfoList?: Array<TSResellerVolumeDiscountInfo>;
    availableSeats?: number;
    availableSeatsInSection?: number;
    ticketValidFrom?: Date;
    ticketValidTo?: Date;
    minimumAmount?: number;
    maximumAmount?: number;
    step?: number;
    requiresCapacitySlot?: boolean;
    salesChannelPerformanceSectionPriceTypeKey?: string;
}