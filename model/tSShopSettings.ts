/**
 * STC.Tickets
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

export interface TSShopSettings { 
    googleTagManagerContainerID?: string;
    googleAnalyticsTrackingID?: string;
    showCalendar?: boolean;
    calendarType?: number;
    showCalendarTimeslot?: boolean;
    calendarTimeslotType?: number;
    enableDiscountCode?: boolean;
    discountCodeType?: number;
    showValidToInOrderLines?: boolean;
    validToInOrderLinesType?: number;
    showFromColumnInOrderLines?: boolean;
}