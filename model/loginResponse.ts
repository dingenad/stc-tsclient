/**
 * STC.Tickets
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */import { ContactInfo } from './contactInfo';


/**
 * Login status information
 */
export interface LoginResponse { 
    /**
     * A list of error messages that occurred during login.
     */
    errorMessages?: Array<string>;
    /**
     * <code>true</code> when the login succeeded; otherwise, <code>false</code>.
     */
    succeeded?: boolean;
    userDetails?: ContactInfo;
    /**
     * The user token to use to finish the subscription.
     */
    userToken?: string;
}