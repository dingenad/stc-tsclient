/**
 * STC.Tickets
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

export interface TicketCodeInfo { 
    ID?: number;
    performanceSectionID?: number;
    subscriptionProductValidityID?: number;
    sectionName?: string;
    ticketCode?: string;
    priceTypeID?: number;
    priceTypeName?: string;
    blocked?: boolean;
    reserved?: boolean;
    maxClaimAmount?: number;
    cachedClaimAmount?: number;
    lastClaimDate?: Date;
    contactID?: number;
    reservationID?: number;
    reservationPriceTypeID?: number;
    reservationPriceTypeKey?: string;
    reservationPriceTypeName?: string;
    reservationPricePerSeat?: number;
    reservationOriginalPrice?: number;
    claimedReservationID?: number;
    claimedReservationPriceTypeID?: number;
    claimedReservationPriceTypeKey?: string;
    claimedReservationPriceTypeName?: string;
    claimedReservationPricePerSeat?: number;
    claimedReservationOriginalPrice?: number;
    reservationNumber?: string;
    reservationConfirmed?: Date;
    reservationContactID?: number;
    gender?: string;
    firstName?: string;
    middle?: string;
    lastName?: string;
    mailAddress?: string;
    phoneNumber?: string;
    mobileNumber?: string;
    eventName?: string;
    performanceName?: string;
    group?: number;
    readonly generalSelectionID?: number;
}