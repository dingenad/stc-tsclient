/**
 * STC.Tickets
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

export interface AllotmentInfo { 
    ID?: number;
    performanceSectionID?: number;
    allotmentNr?: number;
    rowNumber?: string;
    seatNumber?: string;
    ticketCode?: string;
    blocked?: boolean;
    reserved?: boolean;
    reservationID?: number;
    maxClaimAmount?: number;
    cachedClaimAmount?: number;
    lastClaimDate?: Date;
    reservationNumber?: string;
    firstName?: string;
    middle?: string;
    lastName?: string;
    mailAddress?: string;
    phoneNumber?: string;
    mobileNumber?: string;
}