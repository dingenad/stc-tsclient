/**
 * STC.Tickets
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

export interface SubscriptionRenewalInformationModel { 
    renewalTemplateId?: number;
    key?: string;
    subscriptionTemplateKey?: string;
    description?: string;
    tooltip?: string;
    validFrom?: Date;
    validTo?: Date;
    price?: number;
    originalPrice?: number;
    easyRenewal?: boolean;
}