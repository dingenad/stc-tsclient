/**
 * STC.Tickets
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

/**
 * The result of the CreateReservation API call
 */
export interface CreateReservationResponse { 
    /**
     * Returns the reservationId.
     */
    reservationId?: string;
    /**
     * Returns the totalamount of this reservation
     */
    totalAmount?: number;
    /**
     * Only applies on subscription reservations: gives a list of subscriptionkeys of this reservation
     */
    subscriptionKeys?: Array<string>;
    /**
     * Only applies on subscription reservations: gives the url where the subscription can be filled in on the portal
     */
    editSubscriptionUrl?: string;
}