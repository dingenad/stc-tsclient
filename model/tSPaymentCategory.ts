/**
 * STC.Tickets
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */import { TSPaymentMethod } from './tSPaymentMethod';


/**
 * A payment category is a coherent group of payment methods
 */
export interface TSPaymentCategory { 
    /**
     * Enum used to identify category
     */
    category?: TSPaymentCategory.CategoryEnum;
    /**
     * Displayable category name
     */
    name?: string;
    /**
     * Url of category image
     */
    imageUrl?: string;
    /**
     * Desired position when displayed among other payment categories
     */
    sortOrder?: number;
    /**
     * List of payment methods that belong to this category
     */
    paymentMethods?: Array<TSPaymentMethod>;
}
export namespace TSPaymentCategory {
    export type CategoryEnum = 'Unknown' | 'iDeal' | 'CreditCard' | 'Internet' | 'GiftCard' | 'BancontactMrCash' | 'Sofortueberweisung' | 'EPS' | 'Account' | 'Giropay' | 'PayByLink' | 'Budget' | 'KBC';
    export const CategoryEnum = {
        Unknown: 'Unknown' as CategoryEnum,
        IDeal: 'iDeal' as CategoryEnum,
        CreditCard: 'CreditCard' as CategoryEnum,
        Internet: 'Internet' as CategoryEnum,
        GiftCard: 'GiftCard' as CategoryEnum,
        BancontactMrCash: 'BancontactMrCash' as CategoryEnum,
        Sofortueberweisung: 'Sofortueberweisung' as CategoryEnum,
        EPS: 'EPS' as CategoryEnum,
        Account: 'Account' as CategoryEnum,
        Giropay: 'Giropay' as CategoryEnum,
        PayByLink: 'PayByLink' as CategoryEnum,
        Budget: 'Budget' as CategoryEnum,
        KBC: 'KBC' as CategoryEnum
    };
}