/**
 * STC.Tickets
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */import { TSReservationPriceTypeRequestInfo } from './tSReservationPriceTypeRequestInfo';


export interface CreateReservationModel { 
    priceTypeList?: Array<TSReservationPriceTypeRequestInfo>;
    discountCode?: string;
    discountCodes?: Array<string>;
    affiliate?: string;
    externalReservationNumber?: string;
    extraInfo1?: string;
    extraInfo2?: string;
    extraInfo3?: string;
    iPAddress?: string;
    visitDate?: Date;
    continuationLink?: string;
}