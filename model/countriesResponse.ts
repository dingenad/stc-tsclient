/**
 * STC.Tickets
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */import { TSCountryInfo } from './tSCountryInfo';


/**
 * A List of countries that can be used
 */
export interface CountriesResponse { 
    /**
     * The countries that can be used when creating a address
     */
    countries?: Array<TSCountryInfo>;
}