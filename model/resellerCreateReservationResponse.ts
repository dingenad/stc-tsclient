/**
 * STC.Tickets
 * No description provided (generated by Swagger Codegen https://github.com/swagger-api/swagger-codegen)
 *
 * OpenAPI spec version: v1
 * 
 *
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen.git
 * Do not edit the class manually.
 */

export interface ResellerCreateReservationResponse { 
    redirectUrl?: string;
    /**
     * A list of human readable errors concerning the subscription registration.
     */
    errors?: Array<string>;
    /**
     * <code>true</code> when the registration succeeded; otherwise, <code>false</code>.
     */
    readonly success?: boolean;
    /**
     * The Id of the created registration.
     */
    registrationId?: string;
    /**
     * The total amount that needs to be paid for this registration.
     */
    totalAmount?: number;
    /**
     * The key value list of values that need to be posted to the submiturl.
     */
    paymentValues?: { [key: string]: string; };
    /**
     * The url the user should be redirected to. Use the <strong>PaymentValues</strong> as hidden form fields.
     */
    submitUrl?: string;
}